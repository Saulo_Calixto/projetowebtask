/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.webtask.aula.controller.funcional;

import br.com.webtask.aula.domain.model.UserClient;
import br.com.webtask.aula.domain.repo.UserRepo;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import org.hibernate.cfg.Environment;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;

/**
 *
 * @author saulo
 */
//@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")//usa o arquivo de configuração test
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)//startar todo servidor//definindo que vamos usar a porta 8090 do arquivo de conf test 
public class FuncionalTest {

    private WebDriver driver;
    private static final String URL = "http://localhost:8090/login";
    
    @Autowired
    UserRepo userRepo;
    
    @Autowired
    private PasswordEncoder password;
    
    
    @BeforeEach
    public void setUp() {
        UserClient uc1 = UserClient.builder().id(1l)
                        .ativo(true).cpf("123").email("admin@admin")
                        .name("admin").senha(password.encode("123")).build();
        userRepo.save(uc1);
       
        //System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);//vai demorar 2 segundos entre cada passo do teste
        
        try {
            Thread.sleep(3500);
        } catch (InterruptedException ex) {
            Logger.getLogger(FuncionalTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @AfterEach
    public void tearDown() {
        userRepo.deleteAll();
        
        driver.quit();
    }

    @Test
    public void testLoginFalha() throws Exception {
        
        driver.get(URL);
        driver.manage().window().setSize(new Dimension(692, 728));
        //driver.findElement(By.id("username")).click();
        driver.findElement(By.id("username")).sendKeys("abc");
        //driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).sendKeys("abc");
        driver.findElement(By.cssSelector(".login100-form-btn")).click();
        driver.findElement(By.cssSelector(".wrap-login100")).click();
        assertThat(driver.findElement(By.cssSelector(".error")).getText(), is("Login ou Senha incorreta"));
    
    }    
    
    @Test
    public void testLoginSucesso() {
        driver.get(URL);
        driver.manage().window().setSize(new Dimension(697, 735));
        //driver.findElement(By.id("username")).click();
        driver.findElement(By.id("username")).sendKeys("123");
        //driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).sendKeys("123");
        driver.findElement(By.cssSelector(".login100-form-btn")).click();
        //driver.findElement(By.cssSelector("h2")).click();
        assertThat(driver.findElement(By.cssSelector("label")).getText(), is("Olá, admin!"));
  }
    
}
