/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.webtask.aula.controller.service;

import br.com.webtask.aula.domain.model.Task;
import br.com.webtask.aula.domain.model.UserClient;
import br.com.webtask.aula.domain.repo.TaskRepo;
import br.com.webtask.aula.domain.repo.UserRepo;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author saulo
 */
@ExtendWith(SpringExtension.class)        
@ActiveProfiles("test")
public class TaskServiceTest {
//A classe service usa um classe dao e é essa classe dao que vai ser fake 
//service é a classe que se integra com o dao que é o banco de dados 
//O qeue é fake é semrpre a ida ao banco da dados por isso mockamos a dao da classe service
//nos vamos realmente testar os metodos da service mas simulamos a ida no banco da dados
    
    @MockBean//dao fake
    TaskRepo taskRepo;
    
    @MockBean//dao fake
    UserRepo userRepo;
    
    @InjectMocks // injeta os mocks falsos na TaskService 
    TaskService tasks;
    
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);   
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() throws Exception {
        
        //preparar
        //pegar as tarefas do usuario1
        ArrayList<Task> lista = new ArrayList<>();
        lista.add( new Task(1l, "abc", LocalDate.now(), LocalDate.now().plusDays(1), null) );
    
        //o que nos simulamos é a ida ao bando dados que ocorre dentrodo do metodo minha lista
        //nos estamos testando o método minhaLista() e simulamos a findByUserIdOrderByPlannedDateDesc
        //pois a nosso metodo usa esse método que vai no banco da dados 
        Mockito.when(taskRepo.findByUserIdOrderByPlannedDateDesc(1)).thenReturn(lista);
        Mockito.when(userRepo.getOne(1l)).thenReturn(new UserClient(null, "", "", "", "", true, lista));
    
        
        //executar
        List<Task> lista1 = tasks.minhaLista(1);
        
        //validar
        assertEquals(lista, lista1);
        
    }

    @Test
    public void testMetodoParaUsuarioNaoExiste() {
        
        //preparar
        //pegar as tarefas do usuario1
        ArrayList<Task> lista = new ArrayList<>();
        lista.add( new Task(1l, "abc", LocalDate.now(), LocalDate.now().plusDays(1), null) );
    
        //o que nos simulamos é a ida ao bando dados que ocorre dentrodo do metodo minha lista
        //nos estamos testando o método minhaLista() e simulamos a findByUserIdOrderByPlannedDateDesc
        //pois a nosso metodo usa esse método que vai no banco da dados
        //Mockito.when(taskRepo.findByUserIdOrderByPlannedDateDesc(-1)).thenReturn(lista);
        Mockito.when(userRepo.getOne(-1l)).thenReturn(null);
    
        
        //executar
        //Ao passar um usuário que nao existe para o método minha lista o esperado é que esse metodo retorne um erro
        //isso deveria dar um erro então dever ser tratado com try catch e entrar no catch seria o correto considerando
        //que tentamos pagar a lista de um usuário que não existe no caso usuário -1
        //se ele executar a mensagem dentro do try apos a execução do método sem lançar um erro é porque o método esta
        //errado - deveria entrar no catch
        try {
            List<Task> lista1 = tasks.minhaLista(-1);
            fail("Erro - Achou a lista de um usuario que não existente!!!");//errado
        } catch (Exception e) {
            assertTrue(true);//certo
        }
        
    }
    
}
