/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.webtask.aula.controller;

import br.com.webtask.aula.config.security.user.UserLogado;
import br.com.webtask.aula.domain.model.UserClient;
import br.com.webtask.aula.domain.repo.TaskRepo;
import br.com.webtask.aula.domain.repo.UserRepo;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author saulo
 */
//@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@SpringBootTest
//@WebMvcTest(TaskController.class)//informa o controller que será testado nessa classe,
//sobe um servidor web, não teria a camada service nem banco de dados, teriamos que mockar tudo
@AutoConfigureMockMvc
public class TaskControllerTest {
//SpringBootTest ele inicia a aplicação toda ele inicia o banco em memoria, o servidor web, como se o projeto esivesse
//rodando, com uso desse cara nos não precisamos mockar nada pois o banco foi criado o Dao acessa o banco 
//ele roda a aplicação inteira o arquico conf de teste
//@AutoConfigureMockMvc
    //injeta um usuário logado falso
//@WithMockUser
    //injeta um usuario logado real na sesão para nos
//Set<GrantedAuthority> grantedAuthorities  = new HashSet<>();//criando o usuário que vai ser guardado na sessão
//        grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_CLIENTE"));
//        UserLogado ul = new UserLogado("123", "123", grantedAuthorities, 1, "zezin", "ze@ze");
    
//MockMvc variavel usada para fazer o request falso, podemos dizer so funciona devido as novas anotacions ali em cima

    @Autowired
    private MockMvc request;
    
//    @MockBean
//    private TaskService sevice;
//    
//    @MockBean
//    UserRepo userRepo;
//    
//    @MockBean
//    TaskRepo taskRepo;
    
    @Autowired
    private PasswordEncoder password;
    
    @Autowired
    private UserRepo userRepo;
    
    @Autowired
    private TaskRepo taskRepo;

     
    @BeforeEach
    public void setUp() {
        UserClient uc1 = UserClient.builder().id(1l)
                        .ativo(true).cpf("123").email("admin@admin")
                        .name("admin").senha(password.encode("123")).build();
        uc1 = userRepo.save(uc1);
        System.out.println("Criou o usuário :: "+uc1.getId());
    }
    
    @AfterEach
    public void tearDown() {
        taskRepo.deleteAll();
        userRepo.deleteAll();
        
    }

    @Test//acessar pagina não estando logado
    public void testPaginaCreateTask() throws Exception {
        //cenerio

        //determina se a requisição vai ser get ou Post baseado na anotacion em cima do metodo que será testado
        //requisições feitas pelo site são get, se forma requisição de formulario pode ser que seja post
        //simulando um reques via browser para esse endereço - com sei se funcuonou ? pelo status, se retornou
        //o status é 200 se encontro a pagina sem erro o status é 200
        //execução
        ResultActions r = request.perform( MockMvcRequestBuilders.get("/task") );
        
        //verificação
        //rodou e espera
        r.andExpect(MockMvcResultMatchers.status().is(302))
                .andDo(MockMvcResultHandlers.print());
        
    }
    
    @Test //acessar pagina não existe
    @WithMockUser//Mock um usuario logado,com essa anotação ele cria na sessão um usuario logado para gente  
    public void testAcessoPaginaNaoExiste() throws Exception {
        //cenerio

        //execução
        ResultActions r = request.perform( MockMvcRequestBuilders.get("/abc") );
        
        //verificação
        r.andExpect(MockMvcResultMatchers.status().isNotFound())
                .andDo(MockMvcResultHandlers.print());
        
    }
   
    @Test //acessar pagina não existe
    //@WithMockUser//Mock um usuario logado,com essa anotação ele cria na sessão um usuario logado para gente  
    public void testAcessarMinhasTerefas() throws Exception {
        //nessem metodo nos precisariamos do id do usuoario logado o id do usuário logado pois o esse url
        //retorna a lista de tareras do usuario logado, mas o nosso usuario falso com WithMockUser não tem id, 
        //provavelmente vai retorna uma lista vazio
        
        //cenerio
        Set<GrantedAuthority> grantedAuthorities  = new HashSet<>();//criando o usuário que vai ser guardado na sessão
        grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_CLIENTE"));
        UserLogado ul = new UserLogado("123", "123", grantedAuthorities, 1, "zezin", "ze@ze");
        
        //execução
        ResultActions r = request.perform( MockMvcRequestBuilders.get("/task/list")
                .with(SecurityMockMvcRequestPostProcessors.user(ul)) 
        );
        
        //especificar um usuário logado
        //Execute essa requisição com esse usuario logado, usuario ul que criamos agora com id 1 e no banco 
        //vai ter um usuario com id 1 pq inserimos um usuario no beforeeach
        //use isso quando a requisição necesitar de algo do usuário
        //não nos intereça ele retornou o numero de tarefas correto 
//        ResultActions r = request.perform( MockMvcRequestBuilders.get("/task/list")
//                .with(SecurityMockMvcRequestPostProcessors.user(ul)) 
//        );
        
        //verificação
        r.andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
        
    }
    
    @Test //acessar pagina não existe
    //@WithMockUser//Mock um usuario logado,com essa anotação ele cria na sessão um usuario logado para gente  
    public void testarSaveTask() throws Exception {
        //cenerio
        UserClient uc1 = userRepo.findByCpf("123").get();
        
        Set<GrantedAuthority> grantedAuthorities  = new HashSet<>();//criando o usuário que vai ser guardado na sessão
        grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_CLIENTE"));
        UserLogado ul = new UserLogado("123", "123", grantedAuthorities, uc1.getId(), "zezin", "ze@ze");
        
        //execução
        ResultActions r = request.perform( MockMvcRequestBuilders.post("/task")
        .with(SecurityMockMvcRequestPostProcessors.user(ul))
        .with(SecurityMockMvcRequestPostProcessors.csrf()) //no imput do form ele espera alguns campos ocultos esse compando vai passar esse campos
        .param("taskDescription", "abc")
        .param("plannedDate", "2021-09-28"));
        
        //verificação
        r.andExpect(MockMvcResultMatchers.status().isFound())//testando se deu ok
        .andExpect(MockMvcResultMatchers.redirectedUrl("/home"))//testando se retornou a pagina home
        .andDo(MockMvcResultHandlers.print());
        
    }
    
     
    @Test//acessar pagina não estando logado
    public void testLogin() throws Exception {
        //cenerio
        
        //execução
        ResultActions r = request.perform( MockMvcRequestBuilders.post("/login")
        .with(SecurityMockMvcRequestPostProcessors.csrf()) //no imput do form ele espera alguns campos ocultos esse compando vai passar esse campos
        .param("username", "123")
        .param("password", "123"));
        
        //verificação
        r.andExpect(MockMvcResultMatchers.status().isFound());//testando se deu ok
        //.andExpect(MockMvcResultMatchers.view().name("home"))//testando se retornou a pagina home
        //.andDo(MockMvcResultHandlers.print());
        
    }
    
}
