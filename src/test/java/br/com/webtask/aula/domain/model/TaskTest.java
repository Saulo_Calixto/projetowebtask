/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.webtask.aula.domain.model;

import java.time.LocalDate;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;

/**
 *
 * @author daves
 */
public class TaskTest {
    
    public TaskTest() {
    }

    //BDD
    @Test
    public void testSomeMethod() {
        
        //preparo o ambiente
        int a = 1;
        int b = 2;
        
        //executo a ação
        int c = a+b;
                
        //verifico o resultado  c==3
        assertEquals(3, c);
    }
    
    @Test
    public void testSomeMethod2() {
        
        //preparo o ambiente
        int a = 1;
        int b = 2;
        
        //executo a ação
        int c = a*b;
                
        //verifico o resultado  c==3
        assertEquals(2, c);
    }
    
    @Test
    public void testParaVerificarTaskValid() {
        
        //preparo o ambiente
        Task t = new Task();
        t.setTaskDescription("abc abc");
        boolean esperado = false;
        
        //executo a ação
        boolean resultado = t.isDescriptionValid();
                
        //verifico o resultado  c==3
        assertEquals(esperado, resultado);
    }
    
    
    @Test
    @DisplayName("testa se a tarefa está valida com espaço em branco no início")
    public void testParaVerificarTaskValidEspecoNoInicio() {
        
        //preparo o ambiente
        Task t = new Task();
        t.setTaskDescription(" abcabc");
        boolean esperado = false;
        
        //executo a ação
        boolean resultado = t.isDescriptionValid();
                
        //verifico o resultado 
        assertEquals(esperado, resultado);
    }
    
    
    @Test
    @DisplayName("testa se a tarefa está valida contendo números")
    public void testParaVerificarTaskValidComNumeros() {
        
        //preparo o ambiente
        Task t = new Task();
        t.setTaskDescription("abc123");
        boolean esperado = false;
        
        //executo a ação
        boolean resultado = t.isDescriptionValid();
                
        //verifico o resultado 
        assertEquals(esperado, resultado);
    }
    
    @Test
    @DisplayName("testa se a tarefa está valida contendo números no início")
    public void testParaVerificarTaskValidComNumerosNoInicio() {
        
        //preparo o ambiente
        Task t = new Task();
        t.setTaskDescription("111abc");
        boolean esperado = false;
        
        //executo a ação
        boolean resultado = t.isDescriptionValid();
                
        //verifico o resultado 
        assertEquals(esperado, resultado);
    }
    
    @Test
    @DisplayName("testa se a tarefa está valida contendo somente números")
    public void testParaVerificarTaskValidSomenteNumeros() {
        
        //preparo o ambiente
        Task t = new Task();
        t.setTaskDescription("11");
        boolean esperado = false;
        
        //executo a ação
        boolean resultado = t.isDescriptionValid();
                
        //verifico o resultado 
        assertEquals(esperado, resultado);
    }
    
    @Test
    @DisplayName("testa se a tarefa está valida")
    public void testParaVerificarTaskCorreta() {
        
        //preparo o ambiente
        Task t = new Task();
        t.setTaskDescription("abc");
        boolean esperado = true;
        
        //executo a ação
        boolean resultado = t.isDescriptionValid();
                
        //verifico o resultado 
        assertEquals(esperado, resultado);
    }
    
    //------------------------- Atividade proposta em aula -------------------------------------
    
    //método isFinish
    
    @Test
    @DisplayName("Testar se uma tarefa já foi finalizado passando um data anterior a da hoje")
    public void TestTarefaFinalizadaComDataPassada() {
        
        //preparo do ambiente
        Task t = new Task();
        t.setFinishedDate(LocalDate.of(2021, 5, 5));
        boolean esperado = true;
        
        //executo a ação
        boolean resultado = t.isFinish();
                
        //verifico o resultado 
        assertEquals(esperado, resultado);
    }
    
//    @Test
//    @DisplayName("Testar se uma tarefa já foi finalizado passando um data posterios a da hoje")
//    public void TestTarefaFinalizadaComDataFutura() {
//        
//        //preparo do ambiente
//        Task t = new Task();
//        t.setFinishedDate(LocalDate.of(2021, 10, 20));
//        boolean esperado = false;
//        
//        //executo a ação
//        boolean resultado = t.isFinish();
//                
//        //verifico o resultado 
//        assertEquals(esperado, resultado);
//    }
    
    @Test
    @DisplayName("Testar se uma tarefa já foi finalizado passando um data posterios a da hoje")
    public void TestTarefaFinalizadaComDataAtual() {
        
        //preparo do ambiente
        Task t = new Task();
        t.setFinishedDate(LocalDate.now());
        boolean esperado = true;
        
        //executo a ação
        boolean resultado = t.isFinish();
                
        //verifico o resultado 
        assertEquals(esperado, resultado);
    }

    // metodo isLate
    
    @Test
    @DisplayName("Testar se uma tarefa esta atrasada passando uma data anterior a da hoje")
    public void TestTarefaEstaAtrasadaComDataAnteriorHoje() {
        
        //preparo do ambiente
        Task t = new Task();
        t.setPlannedDate(LocalDate.of(2021, 8, 4));
        boolean esperado = true;
        
        //executo a ação
        boolean resultado = t.isLate();
                
        //verifico o resultado 
        assertEquals(esperado, resultado);
    }
    
//    @Test
//    @DisplayName("Testar se uma tarefa esta atrasada passando uma data posterios a da hoje")
//    public void TestTarefaEstaAtrasadaComDataPosteriorHoje() {
//        
//        //preparo do ambiente
//        Task t = new Task();
//        t.setPlannedDate(LocalDate.of(2021, 10, 20));
//        boolean esperado = false;
//        
//        //executo a ação
//        boolean resultado = t.isLate();
//                
//        //verifico o resultado 
//        assertEquals(esperado, resultado);
//    }

//    @Test
//    @DisplayName("Testar se uma tarefa esta atrasada passando a data de hoje")
//    public void TestTarefaEstaAtrasadaComDataDeHoje() {
//        
//        //preparo do ambiente
//        Task t = new Task();
//        t.setPlannedDate(LocalDate.now());
//        boolean esperado = true;
//        
//        //executo a ação
//        boolean resultado = t.isLate();
//                
//        //verifico o resultado 
//        
//        assertEquals(esperado, resultado);
//    }
    
    // getStatus
    
    @Test
    @DisplayName("Testar Status com data planejada para finalização anterior a  data de finalização")
    public void TestStatusComPlannedDateAnteriorAFinishDate() {
        
        //preparo do ambiente
        Task t = new Task();
        t.setPlannedDate(LocalDate.of(2021, 5, 10));
        t.setFinishedDate(LocalDate.of(2021, 8, 10));
        EStatus esperado = EStatus.CONCLUIDO_ATRASADO;
        
        //executo a ação
        EStatus resultado = t.getStatus();
                
        //verifico o resultado 
        
        assertEquals(esperado, resultado);
    }
    
    @Test
    @DisplayName("Testar Status com data planejada para finalização posterios a  data de finalização")
    public void TestStatusComPlannedDatePosteriosAFinishDate() {
        
        //preparo do ambiente
        Task t = new Task();
        t.setPlannedDate(LocalDate.of(2021, 5, 15));
        t.setFinishedDate(LocalDate.of(2021, 5, 10));
        EStatus esperado = EStatus.CONCLUIDO_PRAZO;
        
        //executo a ação
        EStatus resultado = t.getStatus();
                
        //verifico o resultado 
        
        assertEquals(esperado, resultado);
    }
    
    @Test
    @DisplayName("Testar Status com data planejada para finalização igual a  data de finalização")
    public void TestStatusComPlannedDateIgualAFinishDate() {
        
        //preparo do ambiente
        Task t = new Task();
        t.setPlannedDate(LocalDate.of(2021, 8, 10));
        t.setFinishedDate(LocalDate.of(2021, 8, 10));
        EStatus esperado = EStatus.CONCLUIDO_PRAZO;
        
        //executo a ação
        EStatus resultado = t.getStatus();
                
        //verifico o resultado 
        
        assertEquals(esperado, resultado);
    }
    
    @Test
    @DisplayName("Testar Status com data planejada para finalização futura data de finalização Vazia")
    public void TestStatusComPlannedDateFuturaEFinishDateVazia() {
        
        //preparo do ambiente
        Task t = new Task();
        t.setPlannedDate(LocalDate.of(2021, 11, 20));
        t.setFinishedDate(null);
        EStatus esperado = EStatus.NOVO;
        
        //executo a ação
        EStatus resultado = t.getStatus();
                
        //verifico o resultado 
        
        assertEquals(esperado, resultado);
    }
    
    @Test
    @DisplayName("Testar Status com data planejada para finalização passada e data de finalização Vazia")
    public void TestStatusComPlannedDatePassadaEFinishDateVazia() {
        
        //preparo do ambiente
        Task t = new Task();
        t.setPlannedDate(LocalDate.of(2021, 5, 10));
        t.setFinishedDate(null);
        EStatus esperado = EStatus.ATRASADO;
        
        //executo a ação
        EStatus resultado = t.getStatus();
                
        //verifico o resultado 
        
        assertEquals(esperado, resultado);
    }
    
}
