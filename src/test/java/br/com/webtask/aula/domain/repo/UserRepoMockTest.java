/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.webtask.aula.domain.repo;

import br.com.webtask.aula.domain.model.UserClient;
import java.util.Optional;
import javax.transaction.Transactional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author saulo
 */
@ExtendWith(SpringExtension.class)        
@ActiveProfiles("test")
public class UserRepoMockTest {    
//@DataJpaTest isso indica para o spring que eu vou usar testes de banco de dados ele fez como que o spring crie o banco
//@ActiveProfiles("test") Expecifica que essa classe vai usar o arquivo de configuração 
//chamado test (arquivo de conf = profile )
//tudo que rodar aqui vai usar as conf do arquivo application-test.properties
//@MockBean define que o userRepo é falso - então quando chamarmos um metodo dessa classe ele não vai saber o que fazer
//então nos temos que ensinar a ele como responder a chamada de cada método
        
    @MockBean
    private UserRepo userRepo;
    
    private UserClient user1 = new UserClient(null, "Gustin", "321", "gu@gu", "123", false, null);
    private UserClient user2 = new UserClient(null, "Pedrin", "555", "ped@ped", "123", true, null);
    
    public UserRepoMockTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    @Transactional
    public void setUp() {
        MockitoAnnotations.initMocks(this);//comando usado para inicializar os mocks no caso so temos 1 mock no momento
//        userRepo.save(user1);
//        userRepo.save(user2);
    }
    
    @AfterEach
    public void tearDown() {
        userRepo.deleteAll();
    }

    @Test
    public void testSomeMethod() {
        //planejar
        
        UserClient userEsperado = user1;
        
        //ensinando e ele a reagir a chamada de um metodo na nossa classe mockada s
        //ele vai retornar um optional do user1, o metodo get() pertence ao optional e ele vai retornar o user1
        Mockito.when(userRepo.findByName("Gustin")).thenReturn(Optional.of(user1));
        
        //executar
        
        UserClient userObtido = userRepo.findByName("Gustin").get();
        
        assertTrue(!userObtido.isAtivo());
  
       //verificar
       //assertEquals(userEsperado, userObtido);
     
    }
    
    
    @Test
    public void testSomeMethodII() {
        //planejar
        
        UserClient userEsperado = user2;
        
        //ensinando e ele a reagir a chamada de um metodo na nossa classe mockada s
        //ele vai retornar um optional do user1, o metodo get() pertence ao optional e ele vai retornar o user1
        Mockito.when(userRepo.findByName("Pedrin")).thenReturn(Optional.of(user2));
        
        //executar
        
        UserClient userObtido = userRepo.findByName("Pedrin").get();
        
        assertTrue(userObtido.isAtivo());
    
        //verificar
        //assertEquals(userEsperado, userObtido);
     
    }
    
//    @Test
//    @Transactional
//    public void testaSaveNovoUser() {
//        //planejar
//        
//        UserClient user2 = new UserClient(null, "Gustin123", "333", "gu123@gu", "123", true, null);
//        userRepo.save(user2);
//        
//        //executar
//        UserClient userObtido = userRepo.findByName("Gustin123").get();
//        
//        //verificar
//        assertEquals(user2, userObtido);
//    }
//    //não testar apenas os casos em que funciona, teste também casos em que não funciona
//
//    @Test
//    public void testBuscarTodos() {
//        //planejar
//        
//        long userEsperado = 2;
//        
//        //executar
//        
//        long userObtido = userRepo.count();
//        
//        //verificar
//        assertEquals(userEsperado, userObtido);
//    }
    
}
    