/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.webtask.aula.domain.repo;

import br.com.webtask.aula.domain.model.Task;
import br.com.webtask.aula.domain.model.UserClient;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

/**
 *
 * @author saulo
 */
@DataJpaTest
@ActiveProfiles("test")
public class TaskRepoTest {
    
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private TaskRepo taskRepo;
    
    private UserClient user1 = new UserClient(null, "Guto", "321", "gu@gu", "123", true, null);
    private Task task1 = new Task(null, "teste1", LocalDate.of(2021, 10, 10), null, null);
    private Task task2 = new Task(null, "teste2", LocalDate.of(2021, 10, 11), null, null);
    private Task task3 = new Task(null, "teste3", LocalDate.of(2021, 10, 13), null, null);
    
    public TaskRepoTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    @Transactional
    public void setUp() {
        
        userRepo.save(user1);
        UserClient u = userRepo.findByName("Guto").get();
        
        task1.setUser(u);
        task2.setUser(u);
        task3.setUser(u);
        
        taskRepo.save(task1);
        taskRepo.save(task2);
        taskRepo.save(task3);
    
    }
    
    @AfterEach
    public void tearDown() {
        
        userRepo.deleteAll();
        taskRepo.deleteAll();
        
    }

    @Test
    public void testOrdenacaoDecresPassandoIdExiste() {
        
        //planejamento
        UserClient UseTest = userRepo.findByName("Guto").get();
        List<Task> resultadoEsperado = taskRepo.findByTaskDescription("teste3");
        
        //execução
        List<Task> resultadoObtido = taskRepo.findByUserIdOrderByPlannedDateDesc(UseTest.getId());
        
        //verificação
        
        assertEquals(resultadoEsperado.get(0), resultadoObtido.get(0));
        
    }
    
    @Test
    public void testOrdenacaoDecresPassandoIdNaoExiste() {
        
        //planejamento
        List<Task> resultadoEsperado = new ArrayList<>();
        
        //execução
        List<Task> resultadoObtido = taskRepo.findByUserIdOrderByPlannedDateDesc(10);
        
        //verificação
        
        assertEquals(resultadoEsperado, resultadoObtido);
        
    }
    
}
